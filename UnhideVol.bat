@echo off
reg delete HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer /v DisallowCpl /f
reg delete HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl /f
reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer /v SettingsPageVisibility /t REG_SZ /d hide:autoplay;backup;cortana;datausage;easeofaccess-closedcaptioning;easeofaccess-colorfilter;easeofaccess-eyecontrol;easeofaccess-highcontrast;easeofaccess-keyboard;easeofaccess-magnifier;easeofaccess-narrator;easeofaccess-speechrecognition;gaming-broadcasting;gaming-gamebar;gaming-gamedvr;gaming-gamemode;gaming-trueplay;gaming-xboxnetworking;holographic-audio;maps;mobile-devices;network-airplanemode;pen;recovery;remotedesktop;speech;sync;tabletmode;troubleshoot;windowsdefender;windowsinsider;workplace /f
rem reg delete HKEY_CURRENT_USER\SOFTWARE\Policies\Microsoft\Windows\Explorer /v DisableSearchBoxSuggestions /f
exit
