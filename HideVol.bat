@echo off
reg add HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer /v DisallowCpl /t REG_DWORD /d 00000001 /f
reg add HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl /v 1 /t REG_SZ /d "Microsoft.Sound" /f
rem reg add HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl /v 2 /t REG_SZ /d "Microsoft.DevicesAndPrinters" /f

reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer /v SettingsPageVisibility /t REG_SZ /d hide:autoplay;backup;cortana;datausage;easeofaccess-closedcaptioning;easeofaccess-colorfilter;easeofaccess-eyecontrol;easeofaccess-highcontrast;easeofaccess-keyboard;easeofaccess-magnifier;easeofaccess-narrator;easeofaccess-speechrecognition;gaming-broadcasting;gaming-gamebar;gaming-gamedvr;gaming-gamemode;gaming-trueplay;gaming-xboxnetworking;holographic-audio;maps;mobile-devices;network-airplanemode;pen;recovery;remotedesktop;speech;sync;tabletmode;troubleshoot;windowsdefender;windowsinsider;workplace;sound;easeofaccess-audio;taskbar /f
rem reg add HKEY_CURRENT_USER\SOFTWARE\Policies\Microsoft\Windows\Explorer /v DisableSearchBoxSuggestions /t REG_DWORD /d 00000001 /f 
exit
